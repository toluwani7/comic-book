import { FETCH_COMICS, FETCH_PENDING } from '../actions/actionTypes';

const initialState = {
  data: {},
  pending: false,
  match: false,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FETCH_PENDING:
      return {
        ...state,
        pending: action.payload
      };
    case FETCH_COMICS:
      return {
        ...state,
        data: action.payload.data,
        match: action.payload.match
      };
    default:
      return state;
  }
}
