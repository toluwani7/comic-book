import { combineReducers } from 'redux';
import comics from './comicsReducer';

export default combineReducers({
  comics
});
