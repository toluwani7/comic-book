import React from 'react';
import './App.css';
import store from './store';
import { Provider } from 'react-redux';
import './index.css';
import Routes from './routes/index';

class App extends React.Component {
  render() {
    // app wont boot if store is undefined
    if (!store) {
      return null;
    }

    return (
      <Provider store={store}>
        <Routes />
      </Provider>
    );
  }
}

export default App;
