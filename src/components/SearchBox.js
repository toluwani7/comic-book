import React from 'react';
import PropTypes from 'prop-types';
import store from '../store';
import '../components/search.styles.css';
import { requestComics } from '../actions/fetchActions';

class SearchBox extends React.Component {
  constructor(props) {
    super(props);
    this.getComicsById = this.getComicsById.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.state = {
      isValid: true,
    };
  }

  getComicsById() {
    // reset valid input
    this.setState({isValid: true});
    // set number input
    const searchInput = document.querySelector('[type="input"]');

    const validRequest = requestComics(searchInput.value);
    if(!validRequest) {
      this.setState({isValid: false});
    }
  }

  handleChange() {
    clearTimeout(this.timer);
    this.timer = setTimeout(this.getComicsById, 1000);
  }

  handleKeyDown(e) {
    const ENTER_KEY = 13; //enter key code
    if (e.keyCode === ENTER_KEY) {
      return this.getComicsById();
    }
  }

  componentDidMount() {
    this.timer = null
  }

  render() {
    const placeHolder = this.state.isValid ? 'enter number' : 'input is Invalid';
    const errorClass = this.state.isValid ? '' : 'error-search';
    return (
      <input
        className={"search " + errorClass}
        type={"input"}
        placeholder={placeHolder}
        onChange={this.getComicsById}
        onKeyDown={this.handleKeyDown}
      />
    );
  }
}

export default SearchBox;
