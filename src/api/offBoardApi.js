import axios from 'axios';
export const fetchData = url => {
  return axios
    .get(url)
      .then(response => {
          //response is not 200
          if(!response.status === 200){
              return Promise.reject(`Bad Response ${response.status}`)
          }
          return response.data;
      })
    .then(
        response => {
      // Success
      return Promise.resolve(response);
    },
    error => {
        console.warn('error', error);
        return Promise.reject(error);
    });
};
