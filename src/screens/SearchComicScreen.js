import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SearchBox from '../components/SearchBox';
import Navigation from '../components/Navigation';

class SearchComicScreen extends React.Component {
  constructor(props) {
    super(props);
    this.handleMouseHover = this.handleMouseHover.bind(this);
    this.state = {
      isHovering: false
    };
  }

  handleMouseHover() {
    this.setState(this.toggleHoverState);
  }

  toggleHoverState(state) {
    return {
      isHovering: !state.isHovering
    };
  }

  componentDidMount() {}

  render() {
    const { comics, match, isPending } = this.props;
    const hasComic = comics && Object.keys(comics).length > 0 && match;
    const redirectURL = 'https://xkcd.com/' + comics.num;
    console.log('comic data from store', this.props.comics);
    return (
      <div>
        <Navigation />
        <SearchBox />
        {hasComic && !isPending ? (
          <div>
            <p className={'test'}>{comics.title}</p>
            <p className={'test'}>{comics.num}</p>
            {this.state.isHovering && <div>{comics.alt}</div>}
            <a href={redirectURL} target={'_blank'}>
              <img
                onMouseEnter={this.handleMouseHover}
                onMouseLeave={this.handleMouseHover}
                src={comics.img}
                alt={comics.alt}
              ></img>
            </a>
          </div>
        ) : (
          <div>
            {isPending ? (<p className={'test'}>Loading comic....</p>) : !hasComic ? (<p className={'test'}>No comic data found</p>) : (<p className={'test'}>Enter a valid comic ID</p>)}
          </div>
        )}
      </div>
    );
  }
}

SearchComicScreen.defaultProps = {
  comics: {},
}

const mapStateToProps = state => ({
  comics: state.comics.data,
  match: state.comics.match,
  isPending: state.comics.pending
});

export default connect(mapStateToProps)(SearchComicScreen);
