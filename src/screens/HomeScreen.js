import React from 'react';
import { connect } from 'react-redux';
import { fetchComics } from '../actions/fetchActions';
import Navigation from '../components/Navigation';

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.handleMouseHover = this.handleMouseHover.bind(this);
    this.state = {
      isHovering: false
    };
  }

  handleMouseHover() {
    this.setState(this.toggleHoverState);
  }

  toggleHoverState(state) {
    return {
      isHovering: !state.isHovering
    };
  }

  componentDidMount() {
    fetchComics();
  }

  render() {
    const { comics, match, isPending } = this.props;
    const hasComic = comics && Object.keys(comics).length > 0 && match;
    const redirectURL = hasComic ? 'https://xkcd.com/' + comics.num : '';
    return (
      <div>
        <Navigation />
        {hasComic ? (
          <div>
            <p className={'test'}>{comics.title}</p>
            <p className={'test'}>{comics.num}</p>
            {this.state.isHovering && <div>{comics.alt}</div>}
            <a href={redirectURL} target={'_blank'}>
              <img
                onMouseEnter={this.handleMouseHover}
                onMouseLeave={this.handleMouseHover}
                src={comics.img}
                alt={comics.alt}
              ></img>
            </a>
          </div>
        ) : (
          <div>
            {isPending ? (<p className={'test'}>Loading comic....</p>) : (<p className={'test'}>Comic is empty</p>)}
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  comics: state.comics.data
});

export default connect(mapStateToProps)(HomeScreen);
