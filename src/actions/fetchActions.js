import { FETCH_COMICS, FETCH_PENDING } from './actionTypes';
import { fetchData } from '../api/offBoardApi';
import store from '../store/index';

//reset
export const resetComics = () => {
  store.dispatch({
    type: FETCH_COMICS,
    payload: {}
  });
};
// actions

export const requestComics = inputValue => {
    //check if valid input
    if(!inputValue || inputValue === '') {
        return false;
    }
    // convert input value to number
    let comicID = parseInt(inputValue, 10);

    //if comic id value is not number or less than 0 return false
    if(typeof comicID != 'number' || comicID <= 0 || !comicID) {
        return false;
    }
    if (inputValue) {
        store.dispatch({
            type: FETCH_PENDING,
            payload: true,
        });
        setTimeout(() => fetchComics(comicID), 3000);
    }
    return true;
};


export const fetchComics = (
  id = null,
  url = 'https://cors-anywhere.herokuapp.com/http://xkcd.com/info.0.json'
) => {

  if (id !== null) {
    url = `https://cors-anywhere.herokuapp.com/https://xkcd.com/${id}/info.0.json`;
  }
  return fetchData(url)
    .then(
      comics => {
        console.log('data', comics);
        store.dispatch({
          type: FETCH_COMICS,
          payload: { data: comics, match: Object.keys(comics).length > 0}
        });
      store.dispatch({
          type: FETCH_PENDING,
          payload: false,
      });
      },
      error => {
        store.dispatch({
          type: FETCH_COMICS,
          payload: { data: {}, match: false}
        });
        console.log('There has been an error');
          store.dispatch({
              type: FETCH_PENDING,
              payload: false,
          });

      }
    );
};
