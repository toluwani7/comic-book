import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composedWithDevTools } from 'redux-devtools-extension';

import rootReducer from '../reducers';

function configureStore(preloadedState) {
  // const middlewares = [thunkMiddleware];
  // const middlewareEnhancer = applyMiddleware(...middlewares);

  // const enhancer = [middlewareEnhancer];
  // const composedEnhancer = composedWithDevTools(...enhancer);

  return createStore(rootReducer, preloadedState);
}

const store = configureStore();

export default store;
